'use strict';

angular.module('nodeTestApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .state('view', {
        url: '/view/:id',
        templateUrl: 'app/main/view.html',
        controller: 'ViewCtrl',
        resolve: {
          hit: [
            '$stateParams', '$http',
            function($stateParams, $http){
              //TODO - should move to service
              return $http.get('/api/hits/'+$stateParams.id).success(function(data) {
                return data;
              });
            }
          ]
        }
      });
  });