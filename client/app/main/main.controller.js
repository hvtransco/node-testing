(function() {
  'use strict';

  var module = angular.module('nodeTestApp');

  module.controller('MainCtrl', function($scope, $http) {
    $scope.load = function(){
      $http.get('/api/hits').success(function(hits) {
        $scope.hits = hits;
      });
    }

    $scope.search = function() {
      $scope.recentHit = null;
      //try to search weather by location
      $http.post('/api/hits/search', {location: $scope.location}).success(function(data) {
        $scope.recentHit = data;

        //reload
        $scope.load();
      });
    };

    $scope.load();
  });

  module.controller('ViewCtrl', [
    '$scope', 'hit',
    function($scope, hit) {
      $scope.hit = hit.data;
    }
  ]);

})();
