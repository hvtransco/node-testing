'use strict';

var express = require('express');
var controller = require('./hit.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/search', controller.search);
router.delete('/:id', controller.destroy);

module.exports = router;