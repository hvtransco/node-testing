'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var WeatherHitSchema = new Schema({
  location: {type: String, default: ''},
  hit: {type: Number, default: 1},
  day_of_week: String,
  high: Number,
  low: Number,
  high_celsius: Number,
  low_celsius: Number,
  condition: String,
  createdAt: {type: Date, default: Date.now}
});

module.exports = mongoose.model('WeatherHit', WeatherHitSchema);