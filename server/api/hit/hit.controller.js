/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /hits              ->  index
 * POST    /hits              ->  create
 * GET     /hits/:id          ->  show
 * PUT     /hits/:id          ->  update
 * DELETE  /hits/:id          ->  destroy
 */

'use strict';

var _ = require('lodash'),
    request = require('request'),
    Q = require('q');
var Hit = require('./hit.model');

// Get list of hits
exports.index = function(req, res) {
  var start = new Date();
  start.setHours(0,0,0,0);

  //get today only
  var where = {
    createdAt: {
      $gte: start
    }
  };
  Hit.find(where, function (err, hits) {
    if(err) { return handleError(res, err); }
    return res.json(200, hits);
  });
};

// Get a single hit
exports.show = function(req, res) {
  Hit.findById(req.params.id, function (err, hit) {
    if(err) { return handleError(res, err); }
    if(!hit) { return res.send(404); }
    return res.json(hit);
  });
};

// Creates a new hit in the DB.
exports.search = function(req, res) {
  if(!req.body.location){ return res.send(400, {msg: 'Missing location'}); }

  //search
  var options = {
    url: 'https://george-vustrey-weather.p.mashape.com/api.php?location='+encodeURIComponent(req.body.location),
    headers: {
      'X-Mashape-Key': 'lRw5uSScdNmshdqwiG2nvkI4Z664p19p4UtjsnCPYG4z6n2ivL'
    }
  };

  Q.fcall(function(){
    //find hit in the database
    var start = new Date();
    start.setHours(0,0,0,0);
    
    return Hit.findOne({
      location: req.body.location,
      createdAt: {
        $gte: start
      }
    }).exec();
  })
  .then(function(hit){
    var deferred = Q.defer();
    //insert a new hit or update total hit
    if(hit){
      //update total hit
      hit.hit +=1;
      hit.save(function(err){
        if(err){ deferred.reject(new Error('Update error!')); }
        else{
          deferred.resolve(hit);
        }
      });
    }else{
      //try to search data
      request(options, function(error, response, body) {
        if (!error && response.statusCode === 200) {
          var d = new Date();
          var weekday = new Array(7);
          weekday[0]=  "Sun";
          weekday[1] = "Mon";
          weekday[2] = "Tue";
          weekday[3] = "Wed";
          weekday[4] = "Thu";
          weekday[5] = "Fri";
          weekday[6] = "Sat";

          var n = weekday[d.getDay()];
          var weatherArray = JSON.parse(body);
          //find data in the array
          var dataOfToday = null;
          _.each(weatherArray, function(item){
            if(item.day_of_week === n){
              dataOfToday = item;
              return;
            }
          });

          if(!dataOfToday){
            deferred.reject(new Error('Null data!'));
          }else{
            //add locations
            dataOfToday.location = req.body.location;

            Hit.create(dataOfToday, function(err, hit) {
             if(err) { deferred.reject(new Error(err)); }

              deferred.resolve(hit);
            });
          }
        }
      });
    }

    return deferred.promise;
  })
  .then(function(hit){
    //send final data
    return res.json(hit);
  })
  .fail(function (error) {
    console.log(error);
  })
  .done();

};

// Deletes a hit from the DB.
exports.destroy = function(req, res) {
  Hit.findById(req.params.id, function (err, hit) {
    if(err) { return handleError(res, err); }
    if(!hit) { return res.send(404); }
    hit.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}